import 'package:flutter/material.dart';
import 'package:playmedia/tabs/first.dart';
import 'package:playmedia/tabs/second.dart';
import 'package:playmedia/tabs/third.dart';


void main() {
  runApp(MaterialApp(
      // Title
      title: "Using Tabs",
      // Home
      home: MyHome()));
}

class MyHome extends StatefulWidget {
  @override
  MyHomeState createState() => MyHomeState();
}

// SingleTickerProviderStateMixin is used for animation
class MyHomeState extends State<MyHome> with SingleTickerProviderStateMixin {
  // Create a tab controller
  TabController controller;
  
  @override
  void initState() {
    super.initState();

    // Initialize the Tab Controller
    controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Appbar
      appBar: AppBar(
        // Title
        title: Text("It Chiná"),
        // Set the background color of the App Bar
        backgroundColor: Colors.green[900],
      ),
      // Set the TabBar view as the body of the Scaffold
      body: TabBarView(
        // Add tabs as widgets
        children: <Widget>[FirstTab(), SecondTab(), ThirdTab()],
        // set the controller
        controller: controller,
      ),
      // Set the bottom navigation bar
      bottomNavigationBar: Material(
        // set the color of the bottom navigation bar
        color: Colors.green[900],
        // set the tab bar as the child of bottom navigation bar
        child: TabBar(
          tabs: <Tab>[
            Tab(
              // set icon to the tab
              icon: Icon(Icons.home),
              text: 'Inicio',             
            ),
            Tab(
              icon: Icon(Icons.business),
              text: 'Departamentos',
            ),
            Tab(
              icon: Icon(Icons.contact_mail_sharp),
              //text: 'Contacto',
            ),
          ],
          // setup the controller
          controller: controller,
        ),
      ),
    );
  }
}