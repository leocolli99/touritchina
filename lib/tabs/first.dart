//import 'dart:js_util';

import 'package:flutter/material.dart';
//import 'package:playmedia/personal.dart';

class FirstTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.green[100],
      body: Container(
          child: Card(
              color: Colors.green[100],
              elevation: 2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: ListView(
                children: [
                  Image(image: AssetImage('assets/itchinalogo.png')),
                  ListTile(
                    // leading: Icon(Icons.photo_album, color: Colors.blue),
                    title: Text("Misión"),
                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text:
                                    'Formar profesionales humanistas con pertinencia, portadores de conocimientos vanguardistas y competitivos; emprendedores e innovadores a través de una educación superior científica y tecnológica de calidad.'),
                          ]),
                    ),
                  ),
                  ListTile(
                    // leading: Icon(Icons.photo_album, color: Colors.blue),
                    title: Text("Visión"),
                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text:
                                    'Ser una Institución educativa formadora de ciudadanos del mundo a través del desarrollo sostenido sustentable y equitativo. Con personal capacitado con altos estándares de calidad; protectora del medio ambiente e interactuando con las necesidades que exigen los cambios del País y la comunidad.'),
                          ]),
                    ),
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: TextStyle(
                      fontSize: 20,
                      //fontFamily: 'marker',
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                      children: [
                      TextSpan(text: 'Directivos'),
                    ]),
                  ),
                  Image.network('https://i.postimg.cc/2yXhPvQx/Directivos.jpg'),
                  new Padding(padding: new EdgeInsets.all(2.00)),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: TextStyle(
                      fontSize: 20,
                      //fontFamily: 'marker',
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                      children: [
                      TextSpan(text: 'Docentes'),
                    ]),
                  ),
                  Image.network('https://i.postimg.cc/CMvGBy3f/Docentes2.jpg'),
                ],
              ))),
    );
  }
}
