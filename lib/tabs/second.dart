import 'package:flutter/material.dart';
import 'package:playmedia/direccion.dart';
import 'package:playmedia/biblioteca.dart';
import 'package:playmedia/auditorio.dart';
import 'package:playmedia/laboratorio.dart';
import 'package:playmedia/neveria.dart';
import 'package:playmedia/recursosfinancieros.dart';
import 'package:playmedia/controlescolar.dart';
import 'package:playmedia/campodepor.dart';
import 'package:playmedia/canchas.dart';
import 'package:playmedia/granjaporci.dart';
import 'package:playmedia/labalimen.dart';
import 'package:playmedia/labciencanim.dart';
class SecondTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[50],
      body: GridView.count(
 crossAxisCount: 2 ,
 children: [
   Container(
    // Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[
                      900], // background !!!ESTE ES COLOR DEL BOTON SOLAMENTE¡¡¡
                  //onPrimary: Colors.green[900], // foreground !!!!ESTE ONPRIMARY ES PARA CAMBIARLE EL COLOR DE LA LETRA DENTRO DEL BOTON POR ESO LO QUITE ¡¡¡¡¡
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselDemo();
                  }));
                },
                child: Text('Oficinas Administrativas'),
              ),
            //)
   ),
   Container(
     padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new Carousel();
                  }));
                  //Navigator.pop(context, 'Bibli');
                },
                child: Text('Biblioteca'),
              ),
   ),
   Container(
     padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselAuditorio();
                  }));
                  //Navigator.pop(context, 'Audit');
                },
                child: Text('Auditorio'),
              ),
   ),
   Container(
     padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new Carousellaboratorio();
                  }));
                  //Navigator.pop(context, 'lab');
                },
                child: Text('Laboratorio'),
              ),
   ),
   Container(
     padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new Carouselneveria();
                  }));
                  //Navigator.pop(context, 'Nev');
                },
                child: Text('Neveria'),
              ),
   ),
   Container(
     padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new Carouselrecursosfinancieros();
                  }));
                  //Navigator.pop(context, 'recfin');
                },
                child: Text('Recursos Financieros'),
              ),
   ),
   Container(
padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new Carouselcontrolescolar();
                  }));
                  //Navigator.pop(context, 'ctrlesc');
                },
                child: Text('Control Escolar'),
              ),
   ),
   Container(
padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselCampDepor();
                  }));
                  //Navigator.pop(context, 'ctrlesc');
                },
                child: Text('Campo De Futbol'),
              ),
   ),
   Container(
padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselCanchas();
                  }));
                  //Navigator.pop(context, 'ctrlesc');
                },
                child: Text('Canchas Deportivas'),
              ),
   ),
   Container(
padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselGranja();
                  }));
                  //Navigator.pop(context, 'ctrlesc');
                },
                child: Text('Granja Porcina'),
              ),
   ),
   Container(
padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselIndustrias();
                  }));
                  //Navigator.pop(context, 'ctrlesc');
                },
                child: Text('Laboratorio Industrias Alimentarias'),
              ),
   ),
   Container(
padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green[900], // background
                  //onPrimary: Colors.green[900], // foreground
                ),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return new CarouselLabAnimal();
                  }));
                  //Navigator.pop(context, 'ctrlesc');
                },
                child: Text('Laboratorio de ciencia animal'),
              ),
   )
 ],
)
    );
  }
}