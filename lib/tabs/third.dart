import 'package:flutter/material.dart';


class ThirdTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.green[100],
      
      body: Container(
        
          child: Card(
            
              color: Colors.green[100],
              elevation: 2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: ListView(
                children: [
                  new Padding(padding: new EdgeInsets.all(10.00)),
                  ListTile(
                    
                    title: Text("Mensaje"),
                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      
                      text: TextSpan(
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic),
                          children: [
                            TextSpan(
                                text:
                                    '"En estos tiempos dificiles, la tecnologia juega un papel tan importante que incluso es la unica manera para que el mundo no se detenga, la educacion no es una excepción. Es por eso que la tematica de esta App o más bien el objetivo, es el dar a conocer el Tecnologico De Chiná pero de una forma distinta en la cual los aspirantes y sociedad en general puedan dar un vistazo a las bastas areas que el instituto ofrece, todo esto pensando en las muchas dificultades de movilización que existen en nuestro país"'),
                          ]),
                    ),
                  ),
                  new Padding(padding: new EdgeInsets.all(10.00)),
                  Column(
                    children: [
                      ListTile(
                        title: Text('Medios de contacto'),
                      ),
                      ListTile(
                        // leading: Icon(Icons.photo_album, color: Colors.blue),

                        title: Text("Dirección"),

                        subtitle: RichText(
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                              style: TextStyle(color: Colors.black),
                              children: [
                                TextSpan(
                                    text:
                                        'Calle 11 S/N ENTRE 22 Y 28, CHINÁ, CAM. MÉXICO. C.P. 24520'),
                              ]),
                        ),
                      ),
                      ListTile(
                        // leading: Icon(Icons.photo_album, color: Colors.blue),
                        title: Text("Correo Electronico y Numeros Telefonicos"),

                        subtitle: RichText(
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                              style: TextStyle(color: Colors.black),
                              children: [
                                TextSpan(
                                    text:
                                        'Email: dir01_china@tecnm.mxTeléfonos: (981) 82 7 20 81, 82 y 52 Ext. 101 y 103 '),
                              ]),
                        ),
                      ),
                      ListTile(
                        // leading: Icon(Icons.photo_album, color: Colors.blue),

                        title: Text("Facebook"),

                        subtitle: RichText(
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                              style: TextStyle(color: Colors.black),
                              children: [
                                TextSpan(text: '@tecnmcampus.china'),
                              ]),
                        ),
                      ),
                      
                    ],
                    
                  ),
                  
                ],
                
              ),
              
              
              ),
              
              ),

    );
  }
  
}
