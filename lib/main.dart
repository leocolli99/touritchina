import 'dart:async';
//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'departaments.dart';

void main() => runApp(VideoPlayerApp());

class VideoPlayerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IT China',
      home: VideoPlayerScreen(),
    );
  }
}

class VideoPlayerScreen extends StatefulWidget {
  VideoPlayerScreen({Key key}) : super(key: key);

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    // Create and store the VideoPlayerController. The VideoPlayerController
    // offers several different constructors to play videos from assets, files,
    // or the internet.
    _controller = VideoPlayerController.asset(
      'assets/videos/DemoAsus.3gpp',
    );

    // Initialize the controller and store the Future for later use.
    _initializeVideoPlayerFuture = _controller.initialize();

    // Use the controller to loop the video.
    _controller.setLooping(true);

    super.initState();
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        title: Text('Demo Video'),
      ),*/
      // Use a FutureBuilder to display a loading spinner while waiting for the
      // VideoPlayerController to finish initializing.
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            FutureBuilder(
              future: _initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                _controller.play();
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the VideoPlayerController has finished initialization, use
                  // the data it provides to limit the aspect ratio of the video.

                  return AspectRatio(
                    //aspectRatio: _controller.value.aspectRatio,
                    aspectRatio: 56 / 110,
                    // Usa el Widget VideoPlayer para mostrar el vídeo
                    child: VideoPlayer(_controller),
                  );
                } else {
                  // If the VideoPlayerController is still initializing, show a
                  // loading spinner.
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            
            Container(
              
              child: Column(
                
                //manejar info por columnas
                    mainAxisAlignment: MainAxisAlignment.center,
                                /*crossAxisAlignment: CrossAxisAlignment.end,*/
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.all(120.00)), //Para separar un widget de otro
                  Text(
                    'CONOCE EL IT CHINA',
                    style: TextStyle(
                      fontSize: 20,
                      fontFamily: 'marker',
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            
            Container(
            child:FloatingActionButton(
        onPressed: () {
         setState(() {
           if(_controller.value.isPlaying){
             _controller.setVolume(0);
           }
         }); 
         Navigator.push(context, MaterialPageRoute(builder: (context)=>
         MyHome()),);
        },
        child: Text('Ir'),
        //Icon(Icons.navigate_next)
        backgroundColor: Colors.green,
      ),
              alignment: Alignment.bottomCenter,
            ),
          ],
        ),
      ),
    );
  }
}
